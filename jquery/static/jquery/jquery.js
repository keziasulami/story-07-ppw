function changeTheme(){
	if($('.card').hasClass("bg-light")){
		$('.card').removeClass("bg-light");
		$('.card').addClass("bg-info");
	}else{
		$('.card').removeClass("bg-info");
		$('.card').addClass("bg-light");
	}
}
$(document).ready(function(){
    $(".card-body").hide();
	$(".load").remove();
	$("#theme").click(changeTheme);
});
function showCardBody(id) {
	const isActive = $('#' + id).hasClass("active");
	$('.card-body').slideUp();
	$('.card-body').removeClass("active");
	if (isActive){
		$('#' + id).slideUp();
	}else{
		$('#' + id).addClass("active")
		$('#' + id).slideDown();
	}
}