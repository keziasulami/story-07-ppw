from django.test import TestCase, Client
import unittest

# Create your tests here.
class UnitTest(TestCase):

	def test_apakah_ada_url_slash(self):
		c = Client()
		response = c.get('/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_ada_hello(self):		
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn('Hello', content)

	def test_apakah_ada_toggle(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("switch", content)

	def test_apakah_ada_card(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("card", content)

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class FunctionalTest(unittest.TestCase):

	url = 'http://localhost:8000'

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		options.add_argument('--no-sandbox')
		options.add_argument('--disable-dev-shm-usage')
		self.browser = webdriver.Chrome(chrome_options=options)
		self.browser.get(self.url)

	def tearDown(self):
		self.browser.quit()

	def test_judul(self):
		browser = self.browser
		self.assertIn("JQ Kezia", browser.title)

	def test_ganti_tema(self):
		browser = self.browser
		self.assertIn('bg-light', browser.page_source)

		toggle = browser.find_element_by_class_name("slider")
		toggle.click()
		self.assertIn('bg-info', browser.page_source)
